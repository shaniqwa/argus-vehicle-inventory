# ARGUS-Vehicle-Inventory

This project was created for ARGUS. 

Server - node.js

Database (persistent) - MongoDB (on [mLab](https://mlab.com/))

App - Angular 6

## Start project

### app
`cd app/` and `npm i` angular project dependencies

`npm run build` to build angular app to `app/dist/argus-app`

### server
`npm i` on root to install server dependencies

`npm run doc` - create API `doc` folder on project root. Will be served on `http://localhost:3000/doc/`

`npm run start` to start up the server on port 3000








