const express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    path = require('path'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    Vehicle = require('./api/v1/models/vehicle'), //created model loading here
    bodyParser = require('body-parser'),
    logger = require('./log.js');

// mongoose connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://shaniqwa:shani101288@ds217002.mlab.com:17002/argus', { useNewUrlParser: true });


// enable CORS
const corsOption = {
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    exposedHeaders: ['x-auth-token']
};
app.use(cors(corsOption));


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// ----------------------- STATIC -----------------------

app.use('/doc', express.static(__dirname + '/doc'));
app.use(express.static(path.join(__dirname, 'app/dist/argus-app')));


// ----------------------- ROUTES -----------------------

const vehicleRoutes = require('./api/v1/routes/vehicle'); // importing route
app.use('/api/v1/vehicle', vehicleRoutes); // register the route

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

// ----------------------- LISTEN -----------------------

app.listen(port);
logger.info('server listening on port ' + port);

