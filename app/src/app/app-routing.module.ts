import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', loadChildren: './vehicles/vehicles.module#VehiclesModule'}
    ]
  },

  {
    path: '**',
    children: [
      { path: '', loadChildren: './pages/pages.module#PagesModule'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
