import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs/index';
import {Vehicle} from '../../vehicles/vehicle/vehicle.model';
import {catchError, publishLast, refCount} from 'rxjs/internal/operators';
const URL: any = environment.apiUrl;

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {
  }

  // define the error handler in a separate method so we can reuse it in other methods
  private handleError(error: HttpErrorResponse): Observable<any> {
    console.error('ApiService: handleError', error);

    return throwError(error);
  }

  public getAllVehicles(): Observable<Vehicle[]> {
    return this.http.get(URL + '/vehicle')
      .pipe(publishLast(), refCount(), catchError(this.handleError));
  }

  public getVehicleById(id: string): Observable<Vehicle> {
    return this.http.get(URL + '/vehicle/' + id)
      .pipe(publishLast(), refCount(), catchError(this.handleError));
  }

  public addVehicle(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.post(URL + '/vehicle/add/', vehicle)
      .pipe(publishLast(), refCount(), catchError(this.handleError));
  }

  public editVehicle(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.put(URL + '/vehicle/update/' + vehicle.id, vehicle)
      .pipe(publishLast(), refCount(), catchError(this.handleError));
  }

  public deleteVehicle(vehicleId: string): Observable<Vehicle> {
    return this.http.delete(URL + '/vehicle/delete/' + vehicleId)
      .pipe(publishLast(), refCount(), catchError(this.handleError));
  }


}
