import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyMaterialDesignModule} from './my-material-design/my-material-design.module';
import {UpcomingToLatestPipe} from './pipes/orderByDate.pipe';
import {ApiService} from './services/api.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MyMaterialDesignModule,
  ],
  declarations: [
    UpcomingToLatestPipe
  ],
  exports: [
    UpcomingToLatestPipe,
    MyMaterialDesignModule,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ApiService
      ]
    };
  }
}
