import {NgModule} from '@angular/core';
import {
  MAT_DATE_FORMATS,
  MatAutocompleteModule, MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule, MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {MatMomentDateModule} from '@angular/material-moment-adapter';


/**
 * needed for MatDatePicker date formatting
 */
const dateFormat: string = 'DD/MM/YYYY';
export const MY_FORMATS = {
  parse: {
    dateInput: dateFormat,
  },
  display: {
    dateInput: dateFormat,
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  imports: [
    // Material
    MatNativeDateModule,
    MatDatepickerModule,
    MatInputModule,
    // MatRadioModule,
    // MatCheckboxModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatFormFieldModule,
    // MatTooltipModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatChipsModule,
    MatSidenavModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatDialogModule,
    MatGridListModule
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatMomentDateModule,
    MatDatepickerModule,
    MatInputModule,
    MatStepperModule,
    MatRadioModule,
    // MatCheckboxModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    // MatTooltipModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatChipsModule,
    MatBadgeModule,
    MatSidenavModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatDialogModule,
    MatGridListModule
  ],
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ]
})
export class MyMaterialDesignModule {
}
