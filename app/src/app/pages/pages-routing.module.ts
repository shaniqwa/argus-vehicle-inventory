import { Routes, RouterModule } from '@angular/router';

import {Page404Component} from './404/404.component';

export const PagesRoutes: Routes = [
  {
    path: '',
    children: [
      { path: '404', component: Page404Component },
      { path: '**', redirectTo: '/404', pathMatch: 'full' },
    ]
  }
];

export const PagesRoutingModule = RouterModule.forChild(PagesRoutes);
