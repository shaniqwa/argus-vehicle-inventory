import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Page404Component} from './404/404.component';
import {PagesRoutingModule} from './pages-routing.module';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    PagesRoutingModule,
    CommonModule
  ],
  declarations: [
    Page404Component,
  ]
})
export class PagesModule {
}
