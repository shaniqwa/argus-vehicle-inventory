import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Vehicle} from '../vehicle/vehicle.model';

@Component({
  selector: 'app-vehicles-datatable',
  templateUrl: './vehicles-datatable.component.html',
  styleUrls: ['./vehicles-datatable.component.scss']
})
export class VehiclesDatatableComponent implements OnInit {
  private _tableData: Vehicle[];

  @Input()
  public set tableData(vehicles: Vehicle[]) {
    this.isLoading = vehicles === null;
    if (vehicles) {
      this._tableData = vehicles;
      this.dataSource.data = this._tableData;
    }
  }

  @Output() deleteVehicle: EventEmitter<string> = new EventEmitter<string>();
  @Output() addVehicle: EventEmitter<void> = new EventEmitter<void>();

  displayedColumns: string[];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();

  isLoading = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }

  public ngOnInit(): void {
    this.displayedColumns = ['name', 'car_type', 'time_created', 'last_successful_connection', 'action'];
    this.dataSource = new MatTableDataSource(this._tableData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  emitAdd(): void {
    this.addVehicle.emit();
  }

  emitDelete(vehicleId: string): void {
    this.deleteVehicle.emit(vehicleId);
  }
}
