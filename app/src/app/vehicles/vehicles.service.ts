import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/index';
import {Vehicle} from './vehicle/vehicle.model';
import * as _ from 'lodash';
import {ApiService} from '../shared/services/api.service';

@Injectable()
export class VehiclesService {

  private vehiclesSubject: BehaviorSubject<Vehicle[]> = new BehaviorSubject<Vehicle[]>(null);
  vehicles$: Observable<Vehicle[]> = this.vehiclesSubject.asObservable();

  static getCarTypes(): string[] {
    return ['SUV', 'Truck', 'Hybrid'];
  }

  constructor(private api: ApiService) {
    this.loadAllVehicles();
  }

  loadAllVehicles(): void {
    this.api.getAllVehicles().subscribe((vehicles: Vehicle[]) => {
      this.updateVehicles(vehicles);
    });
  }

  updateVehicles(newVehicles: Vehicle[]): void {
    this.vehiclesSubject.next(_.cloneDeep(newVehicles));
  }

  getVehiclesValue(): Vehicle[] {
    return this.vehiclesSubject.getValue();
  }


  getVehicleById(id: string): Observable<Vehicle> {
    return this.api.getVehicleById(id);
  }

  addVehicle(newVehicle: Vehicle): Observable<Vehicle>  {
    const vehicles = this.getVehiclesValue();
    const obs = this.api.addVehicle(newVehicle);
      obs.subscribe((vehicle: Vehicle) => {
      vehicles.push(vehicle);
      this.updateVehicles(vehicles);
    }, (error) => {
      // todo: log error
    });

    return obs;
  }

  editVehicle(editVehicle: Vehicle): Observable<Vehicle> {
    const vehicles = this.getVehiclesValue();
    const i = vehicles.findIndex((vehicle: Vehicle) => vehicle.id === editVehicle.id);
    const obs = this.api.editVehicle(editVehicle);
    if (i > -1) {
      obs.subscribe((vehicle: Vehicle) => {
        vehicles[i] = vehicle;
        this.updateVehicles(vehicles);
      }, (error) => {
        // todo: log error
      });
    }

    return obs;
  }

  deleteVehicle(vehicleId: string): Observable<any> {
    const vehicles = this.getVehiclesValue();
    const i = vehicles.findIndex((vehicle: Vehicle) => vehicle.id === vehicleId);
    const obs = this.api.deleteVehicle(vehicleId);
    if (i > -1) {
      obs.subscribe(() => {
        vehicles.splice(i, 1);
        this.updateVehicles(vehicles);
      }, (error) => {
        // todo: log error
      });
    }

    return obs;
  }

}
