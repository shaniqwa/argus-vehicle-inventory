import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {MyErrorStateMatcher} from '../../shared/errorStateMatcher';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VehiclesService} from '../vehicles.service';
import {Vehicle} from '../vehicle/vehicle.model';

@Component({
  selector: 'app-add-vehicle-dialog',
  templateUrl: './add-vehicle-dialog.component.html',
  styleUrls: ['./add-vehicle-dialog.component.scss']
})
export class AddVehicleDialogComponent implements OnInit {
  form: FormGroup;
  matcher = new MyErrorStateMatcher();
  carTypes: string[];
  constructor(public dialogRef: MatDialogRef<AddVehicleDialogComponent>,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.carTypes = VehiclesService.getCarTypes();
    this.buildForm();
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      name: [, Validators.required],
      car_type: [, Validators.required],
    });
  }

  public submit(): void {
    this.dialogRef.close(new Vehicle(this.form.value));
  }

}
