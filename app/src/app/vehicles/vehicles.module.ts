import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VehicleComponent} from './vehicle/vehicle.component';
import {VehiclesComponent} from './vehicles.component';
import {VehiclesDatatableComponent} from './vehicles-datatable/vehicles-datatable.component';
import {SharedModule} from '../shared/shared.module';
import {VehiclesRoutingModule} from './vehicles-routing.module';
import {VehiclesService} from './vehicles.service';
import {ReactiveFormsModule} from '@angular/forms';
import { AddVehicleDialogComponent } from './add-vehicle-dialog/add-vehicle-dialog.component';

@NgModule({
  imports: [
    VehiclesRoutingModule,
    SharedModule,
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    VehicleComponent,
    VehiclesComponent,
    VehiclesDatatableComponent,
    AddVehicleDialogComponent
  ],
  providers: [
    VehiclesService
  ],
  entryComponents: [
    AddVehicleDialogComponent
  ]
})
export class VehiclesModule {
}
