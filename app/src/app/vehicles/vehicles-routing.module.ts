import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VehiclesComponent} from './vehicles.component';
import {VehicleComponent} from './vehicle/vehicle.component';

export const VehiclesRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: VehiclesComponent
      },
      {
        path: 'vehicle/:id',
        component: VehicleComponent
      },
      {
        path: '**',
        redirectTo: '/404',
        pathMatch: 'full'
      }
    ]
  }
];

export const VehiclesRoutingModule: ModuleWithProviders = RouterModule.forChild(VehiclesRoutes);
