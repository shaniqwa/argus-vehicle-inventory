export class Vehicle {
  id: string;
  name: string;
  car_type: string;
  time_created: number;
  last_successful_connection: number;
  constructor(options: {
    name?: string,
    car_type?: string
  } = {}) {
    this.name = options.name || '';
    this.car_type = options.car_type || '';
  }
}
