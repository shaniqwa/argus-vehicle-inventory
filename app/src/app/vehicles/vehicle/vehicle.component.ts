import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/index';
import {Vehicle} from './vehicle.model';
import {VehiclesService} from '../vehicles.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MyErrorStateMatcher} from '../../shared/errorStateMatcher';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {
  vehicle: Vehicle;
  carTypes: string[];

  form: FormGroup;
  matcher = new MyErrorStateMatcher();
  error: string;

  private sub: Subscription;
  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              public snackBar: MatSnackBar,
              private vehicleService: VehiclesService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.vehicleService.getVehicleById(params['id']).subscribe((vehicle: Vehicle) => {
        this.vehicle = vehicle;
        this.carTypes = VehiclesService.getCarTypes();
        this.buildForm();
      });
    });
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      name: [this.vehicle.name, Validators.required],
      car_type: [this.vehicle.car_type, Validators.required],
    });
  }

  public submit(): void {
    this.error = '';
    this.vehicle.name = this.form.value.name;
    this.vehicle.car_type = this.form.value.type;
    this.vehicleService.editVehicle(this.vehicle).subscribe(() => {
      this.openSnackBar('Vehicle updated successfully');
    }, (error) => {
      this.error = error.message;
    });
  }

  openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
