import { Component, OnInit } from '@angular/core';
import {Vehicle} from './vehicle/vehicle.model';
import {VehiclesService} from './vehicles.service';
import {Observable} from 'rxjs/index';
import {MatDialog, MatSnackBar} from '@angular/material';
import {AddVehicleDialogComponent} from './add-vehicle-dialog/add-vehicle-dialog.component';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
  vehicles$: Observable<Vehicle[]>;
  constructor(private vehiclesService: VehiclesService,
              public snackBar: MatSnackBar,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.vehicles$ = this.vehiclesService.vehicles$;
  }

  public addVehicle(): void {
    const dialogRef = this.dialog.open(AddVehicleDialogComponent);

    dialogRef.afterClosed().subscribe((newVehicle: Vehicle) => {
      this.vehiclesService.addVehicle(newVehicle).subscribe(() => {
        this.openSnackBar('Vehicle ' + newVehicle.name + ' added successfully');
      }, (error) => {
        this.openSnackBar('Vehicle ' + newVehicle.name + ' NOT added');
      });
    });
  }

  public deleteVehicle(vehicleId: string): void {
    this.vehiclesService.deleteVehicle(vehicleId).subscribe(() => {
      this.openSnackBar('Vehicle deleted');
    });
  }

  openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
