'use strict';


const mongoose = require('mongoose'),
    Vehicle = mongoose.model('Vehicle');

const logger = require('../../../log.js');

exports.list_all_vehicles = function(req, res) {
    Vehicle.find({}, function(err, vehicle) {
        if (err) {
            logger.error(err.name + ': ' + err.message);
            res.status(400).json(err);
        }
        res.json(vehicle);
    });
};

exports.create_vehicle = function(req, res) {
    let new_vehicle = new Vehicle(req.body);
    new_vehicle.save(function(err, vehicle) {
        if (err) {
            logger.error(err.name + ': ' + err.message);
            res.status(400).json(err);
        }
        res.json(vehicle);
    });
};


exports.find_vehicle = function(req, res) {
    Vehicle.findById(req.params.vehicleId, function(err, vehicle) {
        if (err) {
            logger.error(err.name + ': ' + err.message);
            res.status(400).json(err);
        }
        res.json(vehicle);
    });
};


exports.update_vehicle = function(req, res) {
    Vehicle.findOneAndUpdate({_id: req.params.vehicleId}, req.body, {new: true}, function(err, vehicle) {
        if (err) {
            logger.error(err.name + ': ' + err.message);
            res.status(400).json(err);
        }
        res.json(vehicle);
    });
};


exports.delete_vehicle = function(req, res) {
    Vehicle.remove({
        _id: req.params.vehicleId
    }, function(err, vehicle) {
        if (err) {
            logger.error(err.name + ': ' + err.message);
            res.status(400).json(err);
        }
        res.json({ message: 'Vehicle successfully deleted' });
    });
};
