'use strict';
const express = require('express'),
    router = express.Router(),
    vehicle = require('../controllers/vehicle');

/**
 * @apiName vehicles
 * @apiGroup Vehicle
 * @api {get} /vehicle
 * @apiDescription get all vehicles
 */
router.get('/',  vehicle.list_all_vehicles);

/**
 * @apiName vehiclesById
 * @apiGroup Vehicle
 * @api {post} /vehicles/:vehicleId
 * @apiDescription get vehicle by id
 * @apiParam {string} vehicleId
 */
router.get('/:vehicleId',  vehicle.find_vehicle);

/**
 * @apiName add
 * @apiGroup Vehicle
 * @api {post} /vehicles/add
 * @apiDescription create a vehicle
 * @apiParam {String} name vehicle name
 * @apiParam {String} car_type (Enum) [SUV, Truck, Hybrid]
 */
router.post('/add', vehicle.create_vehicle);

/**
 * @apiName update
 * @apiGroup Vehicle
 * @api {put} /vehicles/update/:vehicleId
 * @apiDescription update a vehicle
 * @apiParam {string} vehicleId
 */
router.put('/update/:vehicleId',  vehicle.update_vehicle);

/**
 * @apiName delete
 * @apiGroup Vehicle
 * @api {delete} /vehicles/delete/:vehicleId
 * @apiDescription delete a vehicle
 * @apiParam {string} vehicleId
 */
router.delete('/delete/:vehicleId',  vehicle.delete_vehicle);

module.exports = router;
