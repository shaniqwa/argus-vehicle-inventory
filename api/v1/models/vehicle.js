'use strict';
const uuid = require('node-uuid');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

require('mongoose-uuid2')(mongoose);
const UUID = mongoose.Types.UUID;

const VehicleSchema = new Schema({
    _id: { type: UUID, default: uuid.v4 },
    name: {
        type: String,
        required: 'Kindly enter the name of the vehicle'
    },
    car_type: {
        type: String,
        enum: ['SUV', 'Truck', 'Hybrid']
    },
    time_created: {
        type: Date,
        default: Date.now
    },
    last_successful_connection: {
        type: Date,
        default: Date.now
    },
});

VehicleSchema.set('toObject', {getters: true, virtuals: true});
VehicleSchema.set('toJSON', {
    getters: true,
    virtuals: true
});

VehicleSchema.options.toJSON.transform = function (doc, ret, options) {
    ret.time_created = ret.time_created.getTime();
    ret.last_successful_connection = ret.last_successful_connection.getTime();
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
}

module.exports = mongoose.model('Vehicle', VehicleSchema);
